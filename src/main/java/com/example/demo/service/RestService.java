package com.example.demo.service;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.client.RestClient;

@Service
public class RestService {

	private final Logger logger;
	private final RestClient restClient;

	@Autowired
	public RestService(final Logger logger, final RestClient restClient) {
		this.logger = logger;
		this.restClient = restClient;
	}
	
	public void createRequests() {
		logger.info("Creating requests to demo.");

		restClient.get();
		restClient.post();
		restClient.postWithHeadersAndParams();
	}
}
