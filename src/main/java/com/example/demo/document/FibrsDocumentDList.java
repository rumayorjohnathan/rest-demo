package com.example.demo.document;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@IdClass(FibrsDocumentDListId.class)
@Table(name = "FIBRS_DOCUMENT_DLIST")
public class FibrsDocumentDList implements Serializable {

	private static final long serialVersionUID = -2666409028466874613L;

	@Id
	@ManyToOne
	@JoinColumn(name = "Document_Reference_Id", referencedColumnName = "Document_Reference_Id")
	private FibrsDocumentMList fibrsDocumentMList;

	@Id
	@Column(name = "LOB", nullable = false)
	private String lob;

	@Id
	@Column(name = "Product_Type_Code")
	private String productTypeCode;

	@Id
	@Column(name = "Product_Sub_Type_Code")
	private String productSubTypeCode;

	@Id
	@Column(name = "Transaction_Code")
	private String transactionCode;

	@Column(name = "Document_Instruction_EN")
	private String documentInstructionEN;

	@Column(name = "Document_Instruction_FR")
	private String documentInstructionFR;

	@Column(name = "Document_Group_EN")
	private String documentGroupEN;

	@Column(name = "Document_Group_FR")
	private String documentGroupFR;

	@Column(name = "Document_Level_Type")
	private String documentLevelType;

	public FibrsDocumentMList getFibrsDocumentMList() {
		return fibrsDocumentMList;
	}

	public void setFibrsDocumentMList(FibrsDocumentMList fibrsDocumentMList) {
		this.fibrsDocumentMList = fibrsDocumentMList;
	}

	public String getLob() {
		return lob;
	}

	public void setLob(String lob) {
		this.lob = lob;
	}

	public String getProductTypeCode() {
		return productTypeCode;
	}

	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}

	public String getProductSubTypeCode() {
		return productSubTypeCode;
	}

	public void setProductSubTypeCode(String productSubTypeCode) {
		this.productSubTypeCode = productSubTypeCode;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public String getDocumentInstructionEN() {
		return documentInstructionEN;
	}

	public void setDocumentInstructionEN(String documentInstructionEN) {
		this.documentInstructionEN = documentInstructionEN;
	}

	public String getDocumentInstructionFR() {
		return documentInstructionFR;
	}

	public void setDocumentInstructionFR(String documentInstructionFR) {
		this.documentInstructionFR = documentInstructionFR;
	}

	public String getDocumentGroupEN() {
		return documentGroupEN;
	}

	public void setDocumentGroupEN(String documentGroupEN) {
		this.documentGroupEN = documentGroupEN;
	}

	public String getDocumentGroupFR() {
		return documentGroupFR;
	}

	public void setDocumentGroupFR(String documentGroupFR) {
		this.documentGroupFR = documentGroupFR;
	}

	public String getDocumentLevelType() {
		return documentLevelType;
	}

	public void setDocumentLevelType(String documentLevelType) {
		this.documentLevelType = documentLevelType;
	}

	
}
