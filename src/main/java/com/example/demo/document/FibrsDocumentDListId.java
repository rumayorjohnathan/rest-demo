package com.example.demo.document;

import java.io.Serializable;

public class FibrsDocumentDListId implements Serializable {

	private static final long serialVersionUID = 8065352090872802575L;

	private FibrsDocumentMList fibrsDocumentMList;
	private String lob;
	private String productTypeCode;
	private String productSubTypeCode;
	private String transactionCode;

	public FibrsDocumentMList getFibrsDocumentMList() {
		return fibrsDocumentMList;
	}

	public void setFibrsDocumentMList(FibrsDocumentMList fibrsDocumentMList) {
		this.fibrsDocumentMList = fibrsDocumentMList;
	}

	public String getLob() {
		return lob;
	}

	public void setLob(String lob) {
		this.lob = lob;
	}

	public String getProductTypeCode() {
		return productTypeCode;
	}

	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}

	public String getProductSubTypeCode() {
		return productSubTypeCode;
	}

	public void setProductSubTypeCode(String productSubTypeCode) {
		this.productSubTypeCode = productSubTypeCode;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

}
