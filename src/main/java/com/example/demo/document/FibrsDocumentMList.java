package com.example.demo.document;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "FIBRS_DOCUMENT_MLIST")
public class FibrsDocumentMList implements Serializable {


	private static final long serialVersionUID = 1738027752252219627L;

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "Document_Reference_ID", updatable = false, nullable = false)
	private String documentReferenceId;

	@OneToMany(mappedBy = "fibrsDocumentMList", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	private List<FibrsDocumentDList> fibrsDocumentList;

	@Column(name = "Document_Category", nullable = false)
	private String documentCategory;

	@Column(name = "Document_Form_Number", nullable = false)
	private String documentFormNumber;

	@Column(name = "Document_Title_EN", nullable = false)
	private String documentTitleEN;

	@Column(name = "Document_Title_FR", nullable = false)
	private String documentTitleFR;

	public String getDocumentReferenceId() {
		return documentReferenceId;
	}

	public void setDocumentReferenceId(String documentReferenceId) {
		this.documentReferenceId = documentReferenceId;
	}

	public String getDocumentCategory() {
		return documentCategory;
	}

	public void setDocumentCategory(String documentCategory) {
		this.documentCategory = documentCategory;
	}

	public String getDocumentFormNumber() {
		return documentFormNumber;
	}

	public void setDocumentFormNumber(String documentFormNumber) {
		this.documentFormNumber = documentFormNumber;
	}

	public String getDocumentTitleEN() {
		return documentTitleEN;
	}

	public void setDocumentTitleEN(String documentTitleEN) {
		this.documentTitleEN = documentTitleEN;
	}

	public String getDocumentTitleFR() {
		return documentTitleFR;
	}

	public void setDocumentTitleFR(String documentTitleFR) {
		this.documentTitleFR = documentTitleFR;
	}

}
