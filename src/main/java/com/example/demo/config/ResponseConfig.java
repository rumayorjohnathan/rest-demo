package com.example.demo.config;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.example.demo.exception.MissingHeaderException;

@RestControllerAdvice
public class ResponseConfig {

	@ExceptionHandler(value = { MissingHeaderException.class })
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public String resourceNotFoundException(MissingHeaderException ex, WebRequest request) {

		return "Missing header";
	}
}
