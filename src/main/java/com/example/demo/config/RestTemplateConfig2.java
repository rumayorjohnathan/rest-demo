package com.example.demo.config;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.SSLContext;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig2 {

	@Value("${trust-store:classpath:arquivo.jks}")
	private Resource trustStore;

	@Value("${trust-store-password:abcd1234}")
	private String trustStorePassword;

	@Bean
	public RestTemplate restTemplateWithTrustStore(RestTemplateBuilder builder) throws IOException,
			CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

		final SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(trustStore.getURL(), trustStorePassword.toCharArray()).build();
		final SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(sslContext);

		final HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();

		return builder.requestFactory(() -> new HttpComponentsClientHttpRequestFactory(httpClient)).build();
	}
}
