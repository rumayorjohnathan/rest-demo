package com.example.demo.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.RestService;

@RestController
@RequestMapping("/v1/api")
public class InboundApi {

	private final RestService restService;

	public InboundApi(final RestService restService) {
		this.restService = restService;
	}

	@GetMapping
	public ResponseEntity<?> execute() {
		restService.createRequests();
		return ResponseEntity.ok("TOP");
	}
}
