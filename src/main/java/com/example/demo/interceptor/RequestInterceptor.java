package com.example.demo.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;

import com.example.demo.exception.MissingHeaderException;

public class RequestInterceptor implements HandlerInterceptor {

	@Override
    public boolean preHandle(HttpServletRequest requestServlet, HttpServletResponse responseServlet, Object handler) throws Exception {
		String header = requestServlet.getHeader("REQUESTED_HEADER");
		if(header == null) {
			throw new MissingHeaderException();
		}
		return true;
    }
}
