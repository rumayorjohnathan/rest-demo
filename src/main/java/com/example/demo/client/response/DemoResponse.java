package com.example.demo.client.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DemoResponse implements Serializable {

	private static final long serialVersionUID = -5813215766478191196L;

	@JsonProperty("sucesso")
	private String sucesso;

	public DemoResponse() {
		super();
	}

	public DemoResponse(String sucesso) {
		this();
		this.sucesso = sucesso;
	}

	public String getSucesso() {
		return sucesso;
	}

	public void setSucesso(String sucesso) {
		this.sucesso = sucesso;
	}

	@Override
	public String toString() {
		return "DemoResponse [sucesso=" + sucesso + "]";
	}

}
