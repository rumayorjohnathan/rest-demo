package com.example.demo.client;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.demo.client.response.DemoResponse;

@Service
public class RestClient {
	
	/*
	 * NOTA: Quando utilizamos o m�todo EXCHANGE do restTemplate, o response sempre ficar� contido numa classe HttpEntity, onde estar�o contidos o response payload, headers, etc...
	 * Quando utilizados getForObject ou postForObject, o teremos acesso a apenas o response mapeado no payload.
	 */

	@Value("${mock.url:https://run.mocky.io/v3/e574e288-5d2b-4960-b7b6-a79bbe13d580}")
	private String uri;

	private final Logger logger;
	private final RestTemplate restTemplate;

	public RestClient(final Logger logger, final RestTemplate restTemplate) {
		this.logger = logger;
		this.restTemplate = restTemplate;
	}

	public void get() {
		final DemoResponse resp1 = restTemplate.getForObject(uri, DemoResponse.class);
		final HttpEntity<DemoResponse> resp2 = restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(new Object()), DemoResponse.class);

		logger.info("{} | {}", resp1, resp2.getBody());
	}

	public void post() {
		final DemoResponse resp1 = restTemplate.postForObject(uri, new Object(), DemoResponse.class);
		final HttpEntity<DemoResponse> resp2 = restTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(new Object()), DemoResponse.class);

		logger.info("{} | {}", resp1, resp2.getBody());
	}

	public void postWithHeadersAndParams() {
		//EXEMPLO COM QUERY PARAMETERS
		final HttpEntity<DemoResponse> resp1 = restTemplate.exchange(buildUrlWithParams().toUriString(), HttpMethod.PUT, buildHeadersAndBody(), DemoResponse.class);

		//EXEMPLO COM PATH VARIABLES
		final HttpEntity<DemoResponse> resp2 = restTemplate.exchange(buildUrlWithPathVariables("test").toUriString(), HttpMethod.PUT, buildHeadersAndBody(), DemoResponse.class);

		logger.info("{} | {}", resp1.getBody(), resp2.getBody());
	}

	private HttpEntity<?> buildHeadersAndBody() {
		final HttpHeaders headers = new HttpHeaders();
		headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

		return new HttpEntity<>(new Object(), headers);
	}

	private UriComponentsBuilder buildUrlWithParams() {
		return UriComponentsBuilder.fromHttpUrl(uri).queryParam("origin", "TEST");
	}

	private UriComponents buildUrlWithPathVariables(final String test) {
		return UriComponentsBuilder
				.fromHttpUrl(uri)
				.buildAndExpand(test);
	}


}
